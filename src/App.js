import React, { useState } from 'react';
import Background from './Background';
import Intro from './Intro';
import Sprites from './Sprites';
import './App.css';

function App() {
  const [state, setState] = useState({ first: true, playing: false })
  const handleStart = () => setState({ playing: true })
  const handleDie = () => setState({ playing: false })

  return (
    <div className="App">
      <Background playing={state.playing} />
      {state.playing && <Sprites onDie={handleDie} />}
      {!state.playing && <Intro first={state.first} onStart={handleStart} />}
    </div>
  );
}

export default App;
