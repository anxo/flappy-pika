import React from 'react';
import './Background.css';

function Background({ playing }) {
  return (
    <div className="background">
      <div className="clouds" />
      <div className={'grass' + (playing ? ' playing' : '')} />
    </div>
  )
}

export default Background;
