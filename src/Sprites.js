import React, { useState, useEffect } from 'react';
import './Sprites.css';

const baseAcc = 0.5;
const flySpeed = 5;

const badieSize = 160;
const pikaSize = 200;

function Sprites({ onDie }) {

  const [state, setState] = useState({
    x: 0,
    y: Math.round(window.innerHeight / 2 - pikaSize / 2),
    vy: -10,
    ay: baseAcc,
    badies: []
  });

  useEffect(() => {
    let t;
    const loop = () => {
      setState(s => {
        // Collision check
        let die = false
        if (s.y > window.innerHeight - pikaSize || s.y < 0) die = true
        s.badies.forEach((b, i) => {
          const x = b.x - s.x
          if (x < 100 + pikaSize && x > 100 - badieSize) {
            const dy = b.y - s.y
            if (dy < pikaSize && dy > -badieSize) die = true
          }
        })
        if (die) setTimeout(onDie, 0);
        // Movement
        return {
          ...s,
          x: s.x + flySpeed,
          y: s.y + s.vy,
          vy: s.vy + s.ay,
        };
      });
      t = requestAnimationFrame(loop);
    }
    t = requestAnimationFrame(loop);
    return () => cancelAnimationFrame(t);
  }, [onDie]);

  useEffect(() => {
    const addBadie = () => {
      setState(s => ({
        ...s,
        badies: [
          ...s.badies.filter(i => i.x > s.x - badieSize),
          {
            x: s.x + window.innerWidth,
            y: Math.round(Math.random() * (window.innerHeight - badieSize))
          }
        ]
      }))
    }
    addBadie()
    const t = setInterval(addBadie, 3000);
    return () => clearInterval(t)
  }, [])

  const handleMouseDown = () => {
    setState({ ...state, ay: -baseAcc });
  };

  const handleMouseUp = () => {
    setState({ ...state, ay: baseAcc });
  };

  return (
    <div className="sprites" onMouseDown={handleMouseDown} onMouseUp={handleMouseUp}>
      <div className="pika" style={{ top: state.y + 'px' }}/>
      <div className="badies">
        {state.badies.map((b, i) => {
          const x = b.x - state.x
          return (
            <div key={i} className="badie" style={{ top: b.y + 'px', left: x + 'px'}} />
          )
        })}
      </div>
    </div>
  )
}

export default Sprites;
