import React from 'react';
import './Intro.css';

function Intro({ onStart, first }) {
  return (
    <div className="intro">
      <h1>{first ? "Bienvenido" : "Game Over"}</h1>
      <button onClick={onStart}>
        Jugar!
      </button>
    </div>
  )
}

export default Intro;
